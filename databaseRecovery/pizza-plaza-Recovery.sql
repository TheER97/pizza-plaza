-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 29, 2021 at 03:59 PM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pizza-plaza`
--
CREATE DATABASE IF NOT EXISTS `pizza-plaza` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `pizza-plaza`;

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
CREATE TABLE `customer` (
  `ID` int(11) NOT NULL,
  `firstname` varchar(45) DEFAULT NULL,
  `lastname` varchar(45) DEFAULT NULL,
  `street` varchar(45) DEFAULT NULL,
  `streetnumber` varchar(45) DEFAULT NULL,
  `zip` varchar(45) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `phone` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `extras`
--

DROP TABLE IF EXISTS `extras`;
CREATE TABLE `extras` (
  `ID` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `isChoosable` tinyint(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `extras`
--

INSERT INTO `extras` (`ID`, `name`, `price`, `isChoosable`) VALUES
(1, 'Tomaten', 1, 0),
(2, 'Käse', 1, 0),
(3, 'Sardellen', 1, 1),
(4, 'Oliven', 1, 1),
(5, 'Salami', 0.5, 1),
(6, 'Schinken', 0.5, 1),
(7, 'Champignons', 0.75, 1),
(8, 'Paprika', 0.5, 1),
(9, 'Mozzarella', 1, 1),
(10, 'Basilikum', 0.75, 1),
(11, 'Peperoniwurst', 1, 1),
(12, 'Zwiebeln', 0.5, 1),
(13, 'Knoblauch', 0.5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `orderitems`
--

DROP TABLE IF EXISTS `orderitems`;
CREATE TABLE `orderitems` (
  `ID` int(11) NOT NULL,
  `quantity` int(11) DEFAULT NULL,
  `Order_ID` int(11) NOT NULL,
  `Pizzen_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `orderitem_has_extra`
--

DROP TABLE IF EXISTS `orderitem_has_extra`;
CREATE TABLE `orderitem_has_extra` (
  `ID` int(11) NOT NULL,
  `OrderItems_ID` int(11) NOT NULL,
  `Extras_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `ID` int(11) NOT NULL,
  `timestamp` timestamp NULL DEFAULT NULL,
  `Customer_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pizzas`
--

DROP TABLE IF EXISTS `pizzas`;
CREATE TABLE `pizzas` (
  `ID` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `price` float NOT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pizzas`
--

INSERT INTO `pizzas` (`ID`, `name`, `price`, `description`) VALUES
(4, 'Magherita', 4, 'Eine leckere Käsepizza.'),
(5, 'Napoli', 5.5, 'Ein Mix aus Sardellen und Oliven bieten den ultimativen Flair.'),
(6, 'Salami', 4.7, 'Der Klassiker unter den Pizzen.'),
(7, 'Prosciutto', 4.7, 'Der im Volksmund auch als Schinkenpizza bekannte Klassiker.'),
(8, 'Funghi', 4.7, 'Die beste Wahl für alle Pilzliebhaber.'),
(9, 'Salami-Paprika', 5.5, 'Die beste Kombination zwischen deftig und frisch!'),
(10, 'Tricolore', 5.5, '100% vegetarisch - 100% lecker!'),
(11, 'Diavolo', 5.5, 'Teuflisch scharf!'),
(12, 'Roma', 5.5, 'Schinken und Champignons, köstlich im Steinofen zubereitet.');

-- --------------------------------------------------------

--
-- Table structure for table `pizza_has_extra`
--

DROP TABLE IF EXISTS `pizza_has_extra`;
CREATE TABLE `pizza_has_extra` (
  `ID` int(11) NOT NULL,
  `Pizzen_ID` int(11) NOT NULL,
  `Extras_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `extras`
--
ALTER TABLE `extras`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `orderitems`
--
ALTER TABLE `orderitems`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_OrderItems_Order1_idx` (`Order_ID`),
  ADD KEY `fk_OrderItems_Pizzen1_idx` (`Pizzen_ID`);

--
-- Indexes for table `orderitem_has_extra`
--
ALTER TABLE `orderitem_has_extra`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_OrderItem_has_Extra_OrderItems1_idx` (`OrderItems_ID`),
  ADD KEY `fk_OrderItem_has_Extra_Extras1_idx` (`Extras_ID`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_Order_Customer1_idx` (`Customer_ID`);

--
-- Indexes for table `pizzas`
--
ALTER TABLE `pizzas`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `pizza_has_extra`
--
ALTER TABLE `pizza_has_extra`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_Pizza_has_Extra_Pizzen_idx` (`Pizzen_ID`),
  ADD KEY `fk_Pizza_has_Extra_Extras1_idx` (`Extras_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `extras`
--
ALTER TABLE `extras`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `orderitems`
--
ALTER TABLE `orderitems`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orderitem_has_extra`
--
ALTER TABLE `orderitem_has_extra`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pizzas`
--
ALTER TABLE `pizzas`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `pizza_has_extra`
--
ALTER TABLE `pizza_has_extra`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `orderitems`
--
ALTER TABLE `orderitems`
  ADD CONSTRAINT `fk_OrderItems_Order1` FOREIGN KEY (`Order_ID`) REFERENCES `orders` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_OrderItems_Pizzen1` FOREIGN KEY (`Pizzen_ID`) REFERENCES `pizzas` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `orderitem_has_extra`
--
ALTER TABLE `orderitem_has_extra`
  ADD CONSTRAINT `fk_OrderItem_has_Extra_Extras1` FOREIGN KEY (`Extras_ID`) REFERENCES `extras` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_OrderItem_has_Extra_OrderItems1` FOREIGN KEY (`OrderItems_ID`) REFERENCES `orderitems` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `fk_Order_Customer1` FOREIGN KEY (`Customer_ID`) REFERENCES `customer` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pizza_has_extra`
--
ALTER TABLE `pizza_has_extra`
  ADD CONSTRAINT `fk_Pizza_has_Extra_Extras1` FOREIGN KEY (`Extras_ID`) REFERENCES `extras` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Pizza_has_Extra_Pizzen` FOREIGN KEY (`Pizzen_ID`) REFERENCES `pizzas` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
