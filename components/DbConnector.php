<?php
//$db = parse_ini_file('publicConfig/dBConfig.ini');

class DbConnector
{
    private  $servername = "";
    private  $username = "";
    private  $password = "";
    private  $dbname = "";

    /**
     * DbConnector constructor.
     * @param string $servername
     * @param string $username
     * @param string $password
     * @param string $dbname
     */
    public function __construct()
    {
        $this->servername = "localhost";
        $this->username = "Pizza-admin";
        $this->password = "Pizza_Party";
        $this->dbname = "pizza-plaza";
    }


    function createConn(){
        return new mysqli($this->servername, $this->username, $this->password, $this->dbname);
    }
}