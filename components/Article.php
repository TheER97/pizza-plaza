<?php
include "DbConnector.php";

class Article
{
    private $dbConnector;

    function __construct()
    {
        $this->dbConnector=new DbConnector();
    }


    function getReducedArticle()
    {

        $PizzasArr=$this->getPizzas();
        $num= date("ymd")% count($PizzasArr);
        return $PizzasArr[$num];

    }

    function getPizzas()
    {
        // Create connection
        $conn = $this->dbConnector->createConn();

        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }

        $sql = " SELECT * FROM pizzas";
        $result = $conn->query($sql);
        $pizzasArr = array();

        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                array_push($pizzasArr, $row);
            }
        }
        return $pizzasArr;
    }

    function getPizzaID($PizzaName){
        // Create connection
        $conn =$this->dbConnector->createConn();

        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }

        $sql = " SELECT ID FROM pizzas where pizzas.name=\"".$PizzaName."\"";
        $result = $conn->query($sql);
        $fetch= $result->fetch_assoc();
        return $fetch["ID"];
    }

    function getPizzaPrice($PizzaID){
        // Create connection
        $conn =$this->dbConnector->createConn();

        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }

        $sql = " SELECT price FROM pizzas where ID=" . $PizzaID;
        $result = $conn->query($sql);
        $fetch = $result->fetch_assoc();
        return $fetch["price"];
    }

    function getExtraPrice($extraName){
        // Create connection
        $conn =$this->dbConnector->createConn();

        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }

        $sql = " SELECT price FROM extras where extras.name=\"".$extraName."\"";
        $result = $conn->query($sql);
        $fetch= $result->fetch_assoc();
        return $fetch["price"];
    }

    function getPizzasWithExtras(){

        // Create connection
        $conn =$this->dbConnector->createConn();

        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }

        $sql = "SELECT pizzas.name,pizzas.ID,extras.name, extras.price
                FROM pizzas
                JOIN pizza_has_extra
                On pizzas.ID = pizza_has_extra.Pizzas_ID
                JOIN extras
                on pizza_has_extra.Extras_ID =extras.ID
                WHERE extras.isChoosable = 1";
        $result = $conn->query($sql);
        $array = $result;
        return $array;
    }
}