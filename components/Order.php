<?php
include "DbConnector.php";

class Order
{
    private $orderID;
    private $customerID;
    private $dbConnector;
    public $errormessage;

    function __construct()
    {
        $this->dbConnector=new DbConnector();
    }

    /**
     * @return mixed
     */
    public function getOrderID()
    {
        return $this->orderID;
    }

    /**
     * @param mixed $orderID
     */
    public function setOrderID($orderID)
    {
        $this->orderID = $orderID;
    }

    /**
     * @return mixed
     */
    public function getCustomerID()
    {
        return $this->customerID;
    }

    /**
     * @param mixed $customerID
     */
    public function setCustomerID($customerID)
    {
        $this->customerID = $customerID;
    }



    function createOrder($customer){
        //TODO check if inputs are valid

        $this->createCustomer($customer);
        //checkIf Customer was sucsessfully created
        if(is_int($this->customerID)){

            // Create connection
            $conn = $this->dbConnector->createConn();

            // Check connection
            if ($conn->connect_error) {
                $response =$this->conn->connect_error;
                die("Connection failed: " . $this->conn->connect_error);
            }

            $sql = "
            INSERT INTO `orders`(`Customer_ID`) 
            VALUES ($this->customerID)
            ";
            if ($conn->query($sql) === TRUE) {
                $last_id = $conn->insert_id;
                mysqli_close($conn);
                $this->orderID=$last_id;
            } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
            }

        }else{
            echo "customerID is not a Number:".$this->customerID;
        }
    }


    function createCustomer($customer){
        //Customer Atributes
        $firstname="";
        $lasname="";
        $street="";
        $streetnumber="";
        $zip="";
        $city="";
        $phone=0;

        //parse inputarray
        if (is_array( $customer)){
            $firstname=$customer["firstname"];
            $lasname=$customer["lastname"];
            $street=$customer["street"];
            $streetnumber=$customer["streetnumber"];
            $zip=$customer["zip"];
            $city=$customer["firstname"];
            $phone=$customer["phone"];

            //TODO
            //check if inputs are valid

            // Create connection
            $conn = $this->dbConnector->createConn();

            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $this->conn->connect_error);
            }
                $sql = "INSERT INTO `customer`( `firstname`, `lastname`, `street`, `streetnumber`, `zip`, `city`, `phone`) 
                     VALUES ('$firstname','$lasname','$street','$streetnumber','$zip','$city','$phone')
                     ";
                if ($conn->query($sql) === TRUE) {
                    $last_id = $conn->insert_id;
                    mysqli_close($conn);
                    $this->customerID=$last_id;
                } else {
                    $this->errormessage.= "Error: " . $sql . "<br>" . $conn->error;
                }
        }
    }

        //{"amount":int,"pizza":{"extras":[],"name":String}}
    function createOrderItem($cartItem){
        $amount=$cartItem->amount;
        $name=$cartItem->pizza->name;
        $pizzaID=$this->getPizzaID($name);
        $extras=$cartItem->pizza->extras;
        $orderItemID="";

        //create OrderItem without extras
        // Create connection
        $conn = $this->dbConnector->createConn();

        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }
        $sql = "
        INSERT INTO `orderitems`(`quantity`, `Order_ID`, `Pizzas_ID`) 
        VALUES ($amount,$this->orderID,$pizzaID)
        ";
        if ($conn->query($sql) === TRUE) {
            $last_id = $conn->insert_id;
            mysqli_close($conn);
            $orderItemID=$last_id;

        }else {
            $this->errormessage.= "Error: " . $sql . "<br>" . $conn->error;
        }

        //create OrderItem_has_extra for each extra
        foreach ($extras as $extraName){


            // Create connection
            $conn = $this->dbConnector->createConn();

            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }
            $sql = "
                INSERT INTO `orderitem_has_extra`( `OrderItems_ID`, `Extras_ID`) 
                VALUES (".$orderItemID.",".$this->getExtraID($extraName).")
                ";
            if ($conn->query($sql) === False) {
                $this->errormessage.= "Error: " . $sql . "<br>" . $conn->error;
            }
        }
        return $orderItemID;
    }


    function getExtraID($extraName){
        // Create connection
        $conn = $this->dbConnector->createConn();

        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }

        $sql = " SELECT ID FROM extras where extras.name=\"".$extraName."\"";
        $result = $conn->query($sql);
        $fetch= $result->fetch_assoc();
        return $fetch["ID"];
    }

    function getPizzaID($PizzaName){
        // Create connection
        $conn = $this->dbConnector->createConn();

        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }

        $sql = " SELECT ID FROM pizzas where pizzas.name=\"".$PizzaName."\"";
        $result = $conn->query($sql);
        $fetch= $result->fetch_assoc();
        return $fetch["ID"];
    }


    function getOrders(){
        // Create connection
        $conn = $this->dbConnector->createConn();

        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }
        $sql = "
        SELECT orders.ID,firstname, lastname,street,streetnumber,zip,city,phone FROM `orders`
        JOIN customer
        On orders.Customer_ID=Customer.ID
        ";

        $result = $conn->query($sql);
        $orderInfo = array();

        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                array_push($orderInfo, $row);
            }
        }
        return $orderInfo;
    }

    function getOrderItemsOf($Orderid){
        // Create connection
        $conn = $this->dbConnector->createConn();

        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }
        $sql = "
        SELECT * FROM `orderitems` WHERE Order_ID=$Orderid
        ";

        $result = $conn->query($sql);
        $orderItems = array();

        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                array_push($orderItems, $row);
            }
        }
        return $orderItems;
    }

    function getPizzaByID($PizzaId){
        // Create connection
        $conn = $this->dbConnector->createConn();

        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }
        $sql = "
        SELECT * FROM `Pizzas` WHERE ID=$PizzaId
        ";
        $result = $conn->query($sql);
        return $result->fetch_assoc();
    }

    function getExtrasOf($OrderItemId){
        // Create connection
        $conn = $this->dbConnector->createConn();

        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }
        $sql = "
        SELECT extras.name, extras.price FROM `orderitem_has_extra` 
        JOIN extras
        On orderitem_has_extra.Extras_ID=extras.ID
        WHERE orderitem_has_extra.OrderItems_ID='$OrderItemId'
        ";

        $result = $conn->query($sql);
        $extras = array();

        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                array_push($extras, $row);
            }
        }
        return $extras;
    }

    function getOrderItem_Has_Extras($OrderItemId){
        // Create connection
        $conn = $this->dbConnector->createConn();

        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }
        $sql = "
        SELECT * FROM `orderitem_has_extra` 
        WHERE orderitem_has_extra.OrderItems_ID=$OrderItemId
        ";

        $result = $conn->query($sql);
        $has_extras = array();

        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                array_push($has_extras, $row);
            }
        }
        return $has_extras;
    }

    function deleteOrderItem_has_extra_Entry($has_extraID){
        //create connection
        $conn = $this->dbConnector->createConn();
        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }
        $sql = "DELETE FROM `orderitem_has_extra` WHERE `orderitem_has_extra`.`ID` =".$has_extraID;
        $conn->query($sql);
    }

    function deleteOrderItem($orderItemID){
        //create connection
        $conn = $this->dbConnector->createConn();
        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }
        $sql = "DELETE FROM `orderitems` WHERE `orderitems`.`ID` =".$orderItemID;
        $conn->query($sql);
    }

    function deleteOrder($orderId){
        $result="";

        $orderItems = $this->getOrderItemsOf($orderId);
        foreach ($orderItems as $orderItem){

            //Delete the orderitem_has extra entries
            $has_extras=$this->getOrderItem_Has_Extras($orderItem['ID']);
            foreach ($has_extras as $has_extra){
            $this->deleteOrderItem_has_extra_Entry($has_extra['ID']);
            }
            //Delete OrderItems of Order
            $this->deleteOrderItem($orderItem['ID']);
        }
        $conn = $this->dbConnector->createConn();
        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }
        $sql = "DELETE FROM `orders` WHERE `orders`.`ID` =".$orderId;
        $conn->query($sql);

        return $result;
    }
}