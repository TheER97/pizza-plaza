<?php

include "../components/Article.php";

if($_SERVER["REQUEST_METHOD"] == "POST"){
    //TODO
    //check if valid
    //if not send error Response
    if(isset($_POST['cart'])&&isset($_POST['order'])){
        //read HeaderInformation and parse to parameters
        header("Content-Type: application/json; charset=UTF-8");

        $cart = (array) json_decode($_POST["cart"],false);
        $orderItem = json_decode($_POST["order"], false);
        //check if Item is already in Cart

        if (empty($cart)){
            array_push($cart , $orderItem);
        }else {
            $alreadyInCart = false;
            foreach ($cart as $cartItem) {

                if (itemsEqual($orderItem->pizza, $cartItem->pizza)) {
                    $cartItem->amount += $orderItem->amount;
                    $alreadyInCart = true;
                }
            }
            if (!$alreadyInCart) {
                array_push($cart, $orderItem);
            }
        }

        //get price of items from DB
        $article = new Article();
        $price=0;
        foreach ($cart as $cartItem) {
            $price+= ($article->getPizzaPrice($article->getPizzaID($cartItem->pizza->name))*$cartItem->amount);
            foreach ($cartItem->pizza->extras as $extra){
                $price += $article->getExtraPrice($extra)*$cartItem->amount;
            }
        }
        array_push($cart,$price);
        echo json_encode($cart);

    }else {

        http_response_code(400);
        $response="Error: incomplete request";
    }
}


function itemsEqual($firstPiz,$secondPiz){
    if ($firstPiz->name===$secondPiz->name &&$firstPiz->extras===$secondPiz->extras){
        return true;
    }
    return false;
}

