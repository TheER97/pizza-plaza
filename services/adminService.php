<?php

include "../components/Order.php";



// Get all Orders and send as response
if($_SERVER["REQUEST_METHOD"] == "GET"){
    $response="";

    if(isset($_GET["pass"])){
        if($_GET["pass"]="Pizza_Pass"){

        $order=new Order();

        $ordersArr=$order->getOrders();
        $response.="
        <ui >";

        foreach ($ordersArr as $orderinfo) {
            //add Order_id and Customerinfo
            $response.= "
            
            <li class='list-group-item container '>
            <div class='row'>
                <div class='col - sm'><h3 class='orderID'> Bestellung NR:".$orderinfo['ID']."</h3></div>
                <div class='col - sm'><button class='btn btn-danger' onclick='deleteOrder(".$orderinfo['ID'].")'> Abgeschlossen</button></div>
            </div>
            <div class='row'>
                <div class='customerinfo col - sm'>
                <ul class='list-group-item'>
                    <h4>Kundendaten:</h4>
                    <li>Vorname:".$orderinfo['firstname']."</li>
                    <li>Nachname:".$orderinfo['lastname']."</li>
                    <li>Straße:".$orderinfo['street']."</li>
                    <li>Hausnummer:".$orderinfo['streetnumber']."</li>
                    <li>PLZ:".$orderinfo['zip']."</li>
                    <li>Stadt:".$orderinfo['city']."</li>
                    <li>Tel:".$orderinfo['phone']."</li>
                </ul>
                </div>
                <div class='customerinfo col - sm'>
                <ul class='list-group-item'>
                    <h4>Bestellung:</h4>
            
            ";

            $itemsArr=$order->getOrderItemsOf($orderinfo['ID']);
            foreach ($itemsArr as $item) {
                $pizza=$order->getPizzaByID($item['Pizzas_ID']);
                $extrasArr=$order->getExtrasOf($item['ID']);

                $response .= "
                <li>
                   ".$pizza['name']." X ".$item['quantity']."
                </li>
                ";
                foreach ($extrasArr as $extra){
                    $response .= " <div>".$extra['name']."</div>";
                }
            }
            $response.="
                </div>
             </li>";
        }
        $response.="
        </ui> 
         ";

        }else{$response="wrong pass";}
    }else{$response="no pass";}
    echo json_encode($response);
}


//Delete Order with the ID of..

if($_SERVER["REQUEST_METHOD"] == "POST"&& isset($_POST['DelOrderID'])){
    $response="";
    $order=new Order();
    $response.=$ordersArr=$order->deleteOrder($_POST['DelOrderID']);
    echo json_encode($response);
}

if($_SERVER["REQUEST_METHOD"] == "POST"&& !(isset($_POST['DelOrderID']))){echo  json_encode("DelOrderID not set");}