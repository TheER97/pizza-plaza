<?php
include "../components/Order.php";

//GET_Statments
if($_SERVER["REQUEST_METHOD"] == "GET"){

    if(isset($_GET["cart"])){
    $cart = (array) json_decode($_GET["cart"],false);
    $response='';
    foreach ($cart as $cartItem){
    $response .='
<li class="list-group-item pizzas">
        <h1 class="shop-items pizzaName">'. $cartItem->pizza->name .'</h1>';
        if(count($cartItem->pizza->extras) > 0) {
            $response .='<div>Extras: </div>';
    foreach ($cartItem->pizza->extras as $extra){
            $response .='
        <div class="extraName">'.$extra.'</div>';
        }}
        $response .='
        <input type="number" min="1" max="99" value="'.$cartItem->amount.'" class="quantityInput amount">
        <button class="removeBT">Remove</button>
    </li>
    ';
    }
    echo $response;
    }
}

//POST-Statments
//POST new Order
if($_SERVER["REQUEST_METHOD"] == "POST"){
    $order=new Order();

    //Customer POST
    if(isset($_POST['cust'])){
        $order->createOrder((array)json_decode($_POST['cust']));
    }else{
        http_response_code(400);
        echo"Error: incomplete request: Customer is not set";
    }

//POST OrderItems
    if(isset($_POST['cart'])&&isset($_POST['cust'])){
     $response="";

        //parse Data
        $cart= (array) json_decode( $_POST['cart']);
        foreach ($cart as $cartItem){
            $res=$order->createOrderItem($cartItem);
            $response .="OrderItem:#".$res.$order->errormessage." ";
        }
    echo $response;
    }
}