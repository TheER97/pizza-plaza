<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pizza Plaza</title>
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/main.css">
    <link rel="stylesheet" href="assets/css/purecookie.css"/>
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="?site=main">Pizza Plaza</a>
    <div id="itemCountDescr" hidden>Stück:<div id="itemCount"></div></div>
    <div id="itemPriceDescr" hidden>Preis:<div id="itemPrice"></div></div>
    <a class="btn btn-primary" id="checkoutBt" href="?site=checkout" hidden>Checkout</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item <?php if($currentSite === 'main') echo 'active'; ?>">
                <a class="nav-link" href="?site=main">Startseite</a>
            </li>
            <li class="nav-item <?php if($currentSite === 'about') echo 'active'; ?>">
                <a class="nav-link" href="?site=about">&Uuml;ber uns</a>
            </li>
            <li class="nav-item <?php if($currentSite === 'contact') echo 'active'; ?>">
                <a class="nav-link" href="?site=contact">Kontakt</a>
            </li>
            <li class="nav-item <?php if($currentSite === 'imprint') echo 'active'; ?>">
                <a class="nav-link" href="?site=imprint">Impressum</a>
            </li>
            <li class="nav-item <?php if($currentSite === 'pickOrders') echo 'active'; ?>">
                <a class="nav-link" href="?site=pickOrders">Online-Bestellung</a>
            </li>

        </ul>
    </div>
</nav>
<?php
function getSiteName($currentSite){
    switch ($currentSite){
    case 'about':
        return "Über uns";
        break;
    case 'contact':
        return "Kontakt";
        break;
    case 'imprint':
        return "Impressum";
        break;
    case 'pickOrders':
        return "Online-Bestellung";
        break;
    case 'checkout':
        return "Bestellung";
        break;
    default:
        return NULL;
    }
}

?>
<?php if ($currentSite !== 'main'){
    echo"
    <div aria-label=\"breadcrumb\">
    <ol class=\"breadcrumb\">
        <li class=\"breadcrumb-item\"><a href=\"?site=main\">Pizza Plaza</a></li>";
    /*if ($currentSite == 'checkout') {
        echo"<li class=\"breadcrumb-item\"><a href=\"?site=pickOrders\">Online-Bestellung</a></li>";
    }*/
    echo "<li class=\"breadcrumb-item active\" aria-current=\"page\">".getSiteName($currentSite)."</li>
    </ol>
</div>";
}?>

<div class="container" id="contentContainer">
    <?php include $currentSite . '.php'; ?>
</div>

<script src="assets/js/purecookie.js"></script>

<script src="node_modules/jquery/dist/jquery.min.js"></script>
<script src="node_modules/popper.js/dist/popper.min.js"></script>
<script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="assets/js/main.js"></script>
</body>
</html>
