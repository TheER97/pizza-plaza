<?php
include "components/Article.php";
$article = new Article();
$pizzaList = $article->getPizzas();
$extrasList = $article ->getPizzasWithExtras();
?>
<ul>
        <?php
    foreach ($pizzaList as $pizza){
        echo "
            <li class='list-group-item' id=".$pizza["name"].">
                <h1 class='shop-items' >".$pizza["name"].": ".$pizza["price"]."€</h1>
                <p>".$pizza["description"]."</p>";

            foreach ($extrasList as $extras){
                if($extras["ID"]===$pizza["ID"]){
                    echo "<input class='extras' type='checkbox' name=".$extras["name"]." hidden> "."
                <label class='extras' hidden>".$extras["name"].":".$extras["price"]."€"."</label>";
                }
            }
        echo"
            </li>";
    } ?>
    </ul>
<?php
?>
<script src="assets/js/order.js"></script>