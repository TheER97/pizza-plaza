<?php

?>

<!--Einkaufswagen-->

<ul id="cart-list">
</ul>
<script src="assets/js/cartHandler.js"></script>
<!-- Customer Form-->

    <div class="form-group">
        <label for="firstname">Vorname:*</label>
        <input type="text" name="firstname" required="required" class="form-control" id="firstName">
    </div><br>
    <div class="form-group">
        <label for ="lastname">Nachname:*</label>
        <input type="text" name="lastname" required="required" class="form-control" id="lastname">
    </div><br>
    <div class="form-group">
        <label for="street">Straße:</label>
        <input type="text" name="street" class="form-control" id="street">
    </div><br>
    <div class="form-group">
        <label for="streetnumber">Hausnummer:</label>
        <input type="text" name="streetnumber" class="form-control" id="streetnumber">
    </div><br>
    <div class="form-group">
        <label for="zip" >PLZ:</label>
        <input type="text" name="zip" class="form-control" id="zip">
    </div><br>
    <div class="form-group">
        <label for="city">Stadt:</label>
        <input type="text" name="city" class="form-control" id="city">
    </div><br>
    <div class="form-group">
        <label for="phone">Telefonnummer:</label>
        <input type="text" name="phone" class="form-control" id="phone">
    </div>
    <div class="form-group">
        <p>Felder mit "*" müssen angegeben werden</p>
    </div>

<div>
   <button class="form-control btn btn-secondary" id="sendButton" onclick="sendOrder()">Bestellung Absenden</button>
</div>


