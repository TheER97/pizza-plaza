
console.log("loadet fill cart");

fillCart();
//AJAX GET REQUEST to fill CartList
function fillCart() {
var xmlhttp = new XMLHttpRequest();
xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        document.getElementById("cart-list").innerHTML = this.responseText;

        var removeButtons = document.getElementsByClassName("removeBT");
        for (let i = 0; i < removeButtons.length; i++) {
            removeButtons[i].addEventListener('click', function (event) {
                event.target.parentElement.remove()
            });
        }

        var amountInputs = document.getElementsByClassName("amount");
        for (let i = 0; i < amountInputs.length; i++) {
            amountInputs[i].addEventListener('change', function (event) {
                var input = event.target;
                if (isNaN(input.value) || input.value <= 0 || input.value > 99) {
                    input.value = 1;
                }
            });
        }

        var firstNameInput = document.getElementById("firstName");
        firstNameInput.addEventListener('change', checkRequiredFields);
        /*var lastNameInput = document.getElementById("lastname");
        lastNameInput.addEventListener('change', checkRequiredFields);*/
    }};
    xmlhttp.open("GET", "services/checkoutService.php?cart=" + window.sessionStorage.getItem("cart"), true);
    xmlhttp.send();
}

function checkRequiredFields() {
    var firstNameInput = document.getElementById("firstName");
    var lastNameInput = document.getElementById("lastname");
    var sendButton= document.getElementById("sendButton");
    if((!(firstNameInput.value == null))&&(!(firstNameInput.value == "")) && (lastNameInput.value !== null)){
       sendButton.setAttribute('class','form-control btn btn-primary');
    }
}
function requirementsCheck(){
    a=document.getElementById("firstName").value;
    b=document.getElementById("lastname").value;
    if(a!=""&&a!=null&&b!=""&&b!=null)
    {return true}else{return false}
}

function sendOrder() {
    var cart = new Array();
    if(requirementsCheck()){

    var pizzas = document.getElementsByClassName("pizzas");
    var pizzanames= document.getElementsByClassName("pizzaName");
    var inputfields = document.getElementsByClassName("quantityInput amount");
    var  extras= document.getElementsByClassName("extraName");
    if (pizzas != null) {
        //iterates over CartList and filters Data out of Dom
        for (let i = 0; i < pizzas.length; i++) {
            var OrderItem = Object();
            var Pizza = Object();
            Pizza["extras"] = new Array();

            for (let y = 0; y < inputfields.length; y++) {
                if (pizzanames[y].parentElement == pizzas[i]) {
                    Pizza["name"] = pizzanames[y].innerHTML;
                }
            }
            for (let y = 0; y < inputfields.length; y++) {
                if (inputfields[y].parentElement == pizzas[i]) {
                    OrderItem.amount = inputfields[y].value;
                }
            }
            for (let y = 0; y < extras.length; y++) {
                if (extras[y].parentElement == pizzas[i]) {
                    Pizza["extras"].push(extras[y].innerHTML);
                }
            }
            OrderItem.pizza = Pizza;
            cart.push(OrderItem);
        }
    }
     //get Values out of Form and üut into customer Array
        var customer= {
           "firstname": document.getElementById("firstName").value,
           "lastname": document.getElementById("lastname").value,
            "street": document.getElementById("street").value,
            "streetnumber": document.getElementById("streetnumber").value,
            "zip": document.getElementById("zip").value,
            "city": document.getElementById("city").value,
            "phone": document.getElementById("phone").value
        };

    //AJAX POST and load new Page
    var custJSON = JSON.stringify(customer);
    var cartJson=JSON.stringify(cart);

    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var res= this.response;
            window.location.href="?site=thankYou";
        }
    };
    xmlhttp.open("POST", "Services/checkoutService.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send("cust="+custJSON+"&cart="+cartJson);
    }else{
        window.alert("haben Sie einen Namen eingegeben?")
    }

}