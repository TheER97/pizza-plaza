console.log("testCrud loaded");

function testPostCustomer() {
    //AJAX POST random Customer

    var customer={"firstname":getRandomString(5),"lastname":getRandomString(5), "street":getRandomString(5),"streetnumber":getRandomString(5),"zip":getRandomString(5),"city":getRandomString(5),"phone":getRandomInt(15000)};


    var dbParam = JSON.stringify(customer);

    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {

            var res= this.response;
            console.log(res);
        }
    };
    xmlhttp.open("POST", "../Services/checkoutService.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send("cust="+dbParam);

}

function testPostOrder() {
    //AJAX POST examle Order with random generated Customer
    var customer={"firstname":getRandomString(5),"lastname":getRandomString(5), "street":getRandomString(5),"streetnumber":getRandomString(5),"zip":getRandomString(5),"city":getRandomString(5),"phone":getRandomInt(15000)};
    var cartArray= [{"amount":2,"pizza":{"extras":[],"name":"Magherita"}},{"amount":"1","pizza":{"extras":["Sardellen","Oliven"],"name":"Napoli"}},{"amount":"1","pizza":{"extras":["Salami"],"name":"Salami"}}];


    var cust = JSON.stringify(customer);
    var cart=JSON.stringify(cartArray);

    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var res= this.response;
            console.log(res);
        }
    };

    console.log(cart);

    xmlhttp.open("POST", "../Services/checkoutService.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send("cust="+cust+"&cart="+cart);

}

function testGetOrders() {
    var pass= "Pizza_Pass";
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var res = JSON.parse( this.responseText);
            document.getElementById("demo").innerHTML=res;
        }
    };
    xmlhttp.open("GET", "../Services/adminService.php?pass="+pass, true);
    xmlhttp.send();
}


function testDeleteOrder(){
    var pass= "Pizza_Pass";
    var OrderID=document.getElementById('deleteThis').value;


    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var res= JSON.parse( this.responseText);
            console.log(res);
            testGetOrders();
        }
    };
    xmlhttp.open("POST", "../Services/adminService.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send("DelOrderID="+JSON.stringify(OrderID)+"&pass="+pass);

}


function testGetCustomer() {
    //AJAX GET Customer with randID
    var pass = "Pizza_Pass";

    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var myObj = JSON.parse(this.responseText);
            document.getElementById("demo").innerHTML = myObj['name'];
        }
    };
    xmlhttp.open("GET", "Services/adminService.php?pass="+pass, true);
    xmlhttp.send();
}

function getRandomString(length) {
    return    Math.random().toString(20).substr(2,length);
}
function getRandomInt(max) {
    return Math.floor(Math.random() * max);
}
//GET TEMPLATE
function get(){
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var myObj = JSON.parse(this.responseText);
            document.getElementById("demo").innerHTML = myObj['name'];
        }
    };
    xmlhttp.open("GET", "Services/checkoutService.php", true);
    xmlhttp.send();
}

//POST PIZZA
function post(){

    var obj= new Object();
    obj["name"]="demoname";
    obj["extras"]=new Array();
    obj["ammount"]=5;
    var dbParam = JSON.stringify(obj);

    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var res= JSON.parse(this.response);
            console.log(res);
        }
    };
    xmlhttp.open("POST", "Services/orderService.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send("x="+dbParam);
}
