


var pizzaHeaders =  document.getElementsByClassName('shop-items');
for (let i=0; i < pizzaHeaders.length; i++ ){
    var button = pizzaHeaders[i];
    button.addEventListener('click',pizzaClicked);
}

function pizzaClicked(event) {
    var pizzaName = event.target;
    if (pizzaName.parentElement.lastChild.innerHTML !== "x" ){

        var extras=document.getElementsByClassName('extras');
        for (var i=0; i<extras.length;i++){
            if (extras[i].parentElement === pizzaName.parentElement){
                if(extras[i].hidden){extras[i].toggleAttribute("hidden");}
            }
        }

        var inputField = document.createElement("input");
        inputField.type = "number";
        inputField.value = "1";
        inputField.min = "1";
        inputField.max = "99";
        inputField.class = "quantityInput";
        inputField.addEventListener("change", quantityChanged);
        pizzaName.parentElement.appendChild(inputField);

        var addButton = document.createElement("button");
        addButton.innerHTML = "Add";
        addButton.addEventListener("click",addClicked)
        pizzaName.parentElement.appendChild(addButton);

        var closeButton = document.createElement("button");
        closeButton.innerHTML = "x";
        closeButton.addEventListener("click",closeClicked)
        pizzaName.parentElement.appendChild(closeButton);

    }
}

function quantityChanged(event) {
    var input = event.target;
    if( isNaN(input.value)||input.value <=0 || input.value > 99){
        input.value=1;
    }
}
var cart=[];
function addClicked(event){

    //Get Data of Order
    var addButton = event.target;
    var siblings = addButton.parentElement.childNodes;
    var Pizza=new Object;
    var OrderItem=Object();
    Pizza["extras"] = new Array();
    Pizza["name"]=addButton.parentElement.id;
    for (var i = 0; i < siblings.length;i++){
        if (siblings[i].type == "checkbox"){
            if (siblings[i].checked) {
                Pizza["extras"].push(siblings[i].name)
            }
        }
        if (siblings[i].class == "quantityInput"){
            OrderItem.amount = siblings[i].value;
        }
    }
    OrderItem.pizza=Pizza;

// Ajax POST
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            cart= JSON.parse(this.responseText);
            console.log(cart);
            var price=cart.pop();
            var ckBT=document.getElementById("checkoutBt");
            if (ckBT.hidden && !(cart.length===0)){ckBT.toggleAttribute("hidden")}
            window.sessionStorage.setItem("cart", JSON.stringify(cart));
            console.log(window.sessionStorage.getItem("cart"));
            updateItemCount(cart);
            updatePrice(price);
        }
    };
    xmlhttp.open("POST", "services/orderService.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send("order=" + JSON.stringify(OrderItem)+"&cart="+JSON.stringify(cart));


    var extras=document.getElementsByClassName('extras');
    for (let i=0; i<extras.length;i++){
        if (extras[i].parentElement === addButton.parentElement){
            if(!extras.hidden)
                extras[i].toggleAttribute("hidden");
        }
    }
    siblings[siblings.length-3].remove();
    addButton.parentElement.lastChild.remove();
    siblings[siblings.length-1].remove();
}

function closeClicked(event){
    var removeButtonClicked = event.target;
    var siblings = removeButtonClicked.parentElement.childNodes;
    var extras=document.getElementsByClassName('extras');
    for (var i=0; i<extras.length;i++){
        if (extras[i].parentElement === removeButtonClicked.parentElement){
            if(!extras.hidden)
            extras[i].toggleAttribute("hidden");
        }
    }

    siblings[siblings.length-3].remove();
    siblings[siblings.length-2].remove();
    removeButtonClicked.parentElement.lastChild.remove();
}
function updatePrice(price){
    var itemPrice=document.getElementById("itemPrice");
    itemPrice.innerHTML=price;
    var itemPriceDescr=document.getElementById("itemPriceDescr");
    if (itemPriceDescr.hidden){itemPriceDescr.toggleAttribute("hidden")}
}

function updateItemCount(cart) {
    var itemCount=document.getElementById("itemCount");
    var count=0;
    for(let i=0;i<cart.length;i++){
        count += parseInt(cart[i].amount);
    }
    itemCount.innerHTML=count;
    var itemCountDescr=document.getElementById("itemCountDescr");
    if (itemCountDescr.hidden && !(cart.length===0)){itemCountDescr.toggleAttribute("hidden")}
}

