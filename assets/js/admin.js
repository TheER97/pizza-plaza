getOrders();

function getOrders() {
    var pass= "Pizza_Pass";
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var res = JSON.parse( this.responseText);
            document.getElementById("container").innerHTML=res;
        }
    };
    xmlhttp.open("GET", "services/adminService.php?pass="+pass, true);
    xmlhttp.send();
}

function deleteOrder(OrderID){
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var res= JSON.parse( this.responseText);
            console.log(res);
            getOrders();
            alert("Bestellung NR:"+OrderID+"wurde gelöscht");
        }
    };
    xmlhttp.open("POST", "services/adminService.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send("DelOrderID="+JSON.stringify(OrderID));

}